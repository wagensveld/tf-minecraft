locals {
  minecraft_port         = var.minecraft_edition == "java" ? 25565 : 19132
  minecraft_protocol     = var.minecraft_edition == "java" ? "tcp" : "udp"
  minecraft_docker_image = var.minecraft_edition == "java" ? "itzg/minecraft-server" : "itzg/minecraft-bedrock-server"

  efs_port     = 2049
  efs_protocol = "tcp"

  discord_command_handler_name = "minecraft_discord_command_handler"

  minecraft_subdirectory = "/minecraft"
  minecraft_server       = "${var.name}-server"
}
