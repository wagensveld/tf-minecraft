resource "aws_datasync_task" "minecraft_efs_to_s3" {
  name                     = "${var.name}-efs-to-s3"
  destination_location_arn = aws_datasync_location_s3.minecraft_files.arn
  source_location_arn      = aws_datasync_location_efs.efs.arn

  options {
    transfer_mode          = "CHANGED"
    preserve_deleted_files = "PRESERVE"
  }

  includes {
    filter_type = "SIMPLE_PATTERN"
    value       = "/banned-ips.json|/banned-players.json|/ops.json|/usercache.json|/whitelist.json|/server.properties|/server-icon.png|/world"
  }
}

resource "aws_datasync_task" "minecraft_s3_to_efs" {
  name                     = "${var.name}-s3-to-efs"
  destination_location_arn = aws_datasync_location_efs.efs.arn
  source_location_arn      = aws_datasync_location_s3.minecraft_files.arn

  options {
    transfer_mode          = "CHANGED"
    preserve_deleted_files = "PRESERVE"
    gid                    = "NONE"
    uid                    = "NONE"
    posix_permissions      = "NONE"
  }
}

resource "aws_datasync_location_efs" "efs" {
  efs_file_system_arn = aws_efs_file_system.this.arn
  ec2_config {
    security_group_arns = [data.aws_security_group.default.arn]
    subnet_arn          = local.subnet_arn
  }
  subdirectory = local.minecraft_subdirectory
}

resource "aws_datasync_location_s3" "minecraft_files" {
  s3_bucket_arn = aws_s3_bucket.minecraft_files.arn
  s3_config {
    bucket_access_role_arn = aws_iam_role.data_sync_s3.arn
  }
  subdirectory = local.minecraft_subdirectory
}

data "aws_iam_policy_document" "data_sync_s3_trust" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["datasync.amazonaws.com"]
    }
    condition {
      test     = "StringEquals"
      variable = "aws:SourceAccount"
      values   = [data.aws_caller_identity.current.account_id]
    }
    condition {
      test     = "ArnLike"
      variable = "aws:SourceArn"
      values   = ["arn:aws:datasync:${var.region}:${data.aws_caller_identity.current.account_id}:*"]
    }
  }
}

data "aws_iam_policy_document" "data_sync_s3" {
  statement {
    sid = "AWSDataSyncS3BucketPermissions"
    actions = [
      "s3:GetBucketLocation",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads"
    ]
    resources = [aws_s3_bucket.minecraft_files.arn]
  }
  statement {
    sid = "AWSDataSyncS3ObjectPermissions"
    actions = [
      "s3:AbortMultipartUpload",
      "s3:DeleteObject",
      "s3:GetObject",
      "s3:GetObjectTagging",
      "s3:GetObjectVersion",
      "s3:ListMultipartUploadParts",
      "s3:PutObject",
      "s3:PutObjectTagging"
    ]
    resources = ["${aws_s3_bucket.minecraft_files.arn}/*"]
  }
}

resource "aws_iam_policy" "data_sync_s3" {
  name        = "datasync-s3-bucket-access-${var.zone_name}-files"
  path        = "/"
  description = "Policy for AWS DataSync for S3 bucket access"
  policy      = data.aws_iam_policy_document.data_sync_s3.json
}

resource "aws_iam_role" "data_sync_s3" {
  name                = "datasync-s3-${var.zone_name}"
  assume_role_policy  = data.aws_iam_policy_document.data_sync_s3_trust.json
  managed_policy_arns = [aws_iam_policy.data_sync_s3.arn]
}
