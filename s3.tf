resource "aws_s3_bucket" "minecraft_files" {
  bucket = "${var.zone_name}-files"
}

resource "aws_s3_bucket_versioning" "minecraft_files" {
  bucket = aws_s3_bucket.minecraft_files.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "minecraft_files" {
  bucket = aws_s3_bucket.minecraft_files.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "minecraft_files" {
  bucket = aws_s3_bucket.minecraft_files.id
  rule {
    bucket_key_enabled = true
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_intelligent_tiering_configuration" "minecraft_files" {
  bucket = aws_s3_bucket.minecraft_files.id
  name   = "EntireBucket"

  tiering {
    access_tier = "DEEP_ARCHIVE_ACCESS"
    days        = 180
  }
  tiering {
    access_tier = "ARCHIVE_ACCESS"
    days        = 125
  }
}
