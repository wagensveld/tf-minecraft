variable "region" {
  type        = string
  description = "Default AWS region"
  default     = "ap-southeast-2"
}

variable "zone_name" {
  type        = string
  description = "Route53 Zone Name"
  sensitive   = true
}

variable "name" {
  type        = string
  description = "Name for various resources"
  default     = "minecraft"
}

variable "minecraft_edition" {
  type        = string
  description = "Edition of Minecraft to deploy."
  default     = "java"

  validation {
    condition     = contains(["java", "bedrock"], var.minecraft_edition)
    error_message = "Valid values for var: minecraft_edition are (java, bedrock)."
  }
}

variable "minecraft_server_type" {
  type        = string
  description = "Type of Minecraft server to deploy. See: https://docker-minecraft-server.readthedocs.io/en/latest/types-and-platforms/"
  default     = "FABRIC"
}

variable "minecraft_modrinth_projects" {
  type        = string
  description = "Minecraft modrinth projects to include."
  default     = "ferrite-core,lithium,krypton,xaeros-world-map,xaeros-minimap,monsters-in-the-closet,your-items-are-safe,clumps,shulkerboxtooltip,fabric-api,collective"
}

variable "ecs_cpu" {
  type        = number
  description = "CPU value for the ECS task definition."
  default     = 1024

  validation {
    condition     = contains([256, 512, 1024, 2048, 4096, 8192, 16384], var.ecs_cpu)
    error_message = "Valid values for var: ecs_cpu are (256, 512, 1024, 2048, 4096, 8192, 16384)."
  }
}

variable "ecs_memory" {
  type        = number
  description = "Memory value for the ECS task definition."
  default     = 2048

  validation {
    condition     = contains([512, 1024, 2048, 3072, 4096, 5120, 6144, 7168, 8192, 16384, 30720], var.ecs_memory)
    error_message = "Valid values for var: ecs_memory are (512, 1024, 2048, 3072, 4096, 5120, 6144, 7168, 8192, 16384, 30720)."
  }
}

variable "email" {
  type        = string
  description = "Email address to receive SNS topic notifications."
  sensitive   = true
}

variable "discord_public_key" {
  type        = string
  description = "Public key for the Discord bot."
  sensitive   = true
}
