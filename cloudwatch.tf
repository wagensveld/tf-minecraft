resource "aws_cloudwatch_log_metric_filter" "user_uuid" {
  name           = "UserUUID"
  pattern        = "UUID of player"
  log_group_name = aws_cloudwatch_log_group.minecraft_server.name
  metric_transformation {
    name      = "UserUUID"
    namespace = "Minecraft/Users"
    value     = 1
  }
}

resource "aws_cloudwatch_metric_alarm" "user_uuid" {
  alarm_name                = "minecraft-user-auth-alarm"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = 1
  period                    = 600
  metric_name               = aws_cloudwatch_log_metric_filter.user_uuid.name
  namespace                 = "Minecraft/Users"
  statistic                 = "SampleCount"
  insufficient_data_actions = []
  alarm_actions             = [aws_sns_topic.this.arn]
}
