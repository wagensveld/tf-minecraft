resource "aws_secretsmanager_secret" "bot_token" {
  name = "minecraft_discord_bot_token"
}

data "aws_iam_policy_document" "lambda_discord_bot_token" {
  statement {
    actions = [
      "secretsmanager:GetSecretValue"
    ]
    resources = [
      aws_secretsmanager_secret.bot_token.arn
    ]
  }
}

resource "aws_iam_policy" "lambda_discord_bot_token" {
  name        = "lambda-discord-bot-token"
  path        = "/"
  description = "Allows lambda to retreive the Discord bot token"
  policy      = data.aws_iam_policy_document.lambda_discord_bot_token.json
}

data "aws_s3_object" "lambda_discord_command_handler" {
  bucket = aws_s3_bucket.minecraft_files.bucket
  key    = "lambda/${local.discord_command_handler_name}.zip"
}

data "aws_s3_object" "lambda_discord_command_handler_sha" {
  bucket = aws_s3_bucket.minecraft_files.bucket
  key    = "lambda/${local.discord_command_handler_name}.zip.sha256.txt"
}

resource "aws_iam_role" "lambda_discord_command_handler" {
  name               = "iam_for_minecraft_lambda_discord_command_handler"
  assume_role_policy = data.aws_iam_policy_document.lambda_discord.json
  managed_policy_arns = [
    aws_iam_policy.ecs_rw.arn,
    aws_iam_policy.ecs_task_rw.arn,
    aws_iam_policy.lambda_discord_bot_token.arn,
    data.aws_iam_policy.AWSLambdaBasicExecutionRole.arn
  ]
}

resource "aws_cloudwatch_log_group" "lambda_discord_command_handler" {
  name              = "/aws/lambda/${aws_lambda_function.lambda_discord_command_handler.function_name}"
  retention_in_days = 7
}

resource "aws_lambda_function" "lambda_discord_command_handler" {
  s3_bucket         = data.aws_s3_object.lambda_discord_command_handler.bucket
  s3_key            = data.aws_s3_object.lambda_discord_command_handler.key
  s3_object_version = data.aws_s3_object.lambda_discord_command_handler.version_id
  handler           = "${local.discord_command_handler_name}.lambda_handler"
  function_name     = local.discord_command_handler_name
  role              = aws_iam_role.lambda_discord_command_handler.arn
  source_code_hash  = chomp(data.aws_s3_object.lambda_discord_command_handler_sha.body)
  runtime           = "python3.12"
  timeout           = 30

  environment {
    variables = {
      CLUSTER = aws_ecs_cluster.this.name
      SERVICE = aws_ecs_service.this.name
    }
  }
}
