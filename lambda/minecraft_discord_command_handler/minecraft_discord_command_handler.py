"""
Handles the Discord bot's commands.
"""

import json
import os

import boto3
import urllib3

ecs = boto3.client("ecs")
CLUSTER = os.environ["CLUSTER"]
SERVICE = os.environ["SERVICE"]

http = urllib3.PoolManager()


def lambda_handler(event, context):  # pylint:disable=unused-argument
    """
    Interprets the command and forwards it onto the appropriate function.
    """
    body = json.loads(event["body"])
    command = body["data"]["name"]
    if command == "hello":
        return hello(body)
    if command == "status":
        return ecs_status(body)
    if command == "start":
        return ecs_start(body)
    return {"statusCode": 400, "body": json.dumps("unhandled command")}


def hello(body):
    """
    A Hello World function, returns with a greeting to the user.
    """
    user_name = body["member"]["user"]["global_name"]
    return send_message(body, f"Hello {user_name}.")


def ecs_status(body):
    """
    Returns the status of the ECS for Minecraft.
    If it's running it retrieves the IP address.
    """
    response = ecs.describe_services(cluster=CLUSTER, services=[SERVICE])
    running = response["services"][0]["runningCount"]
    pending = response["services"][0]["pendingCount"]

    if running == 0 and pending == 0:
        return send_message(body, "Minecraft server is offline.")
    if running == 1:
        task_arns = ecs.list_tasks(
            cluster=CLUSTER,
            serviceName=SERVICE,
        )["taskArns"]
        for eni in ecs.describe_tasks(cluster=CLUSTER, tasks=task_arns)["tasks"][0][
            "attachments"
        ][0]["details"]:
            if eni["name"] == "networkInterfaceId":
                network_id = eni["value"]
                public_ip = boto3.client("ec2").describe_network_interfaces(
                    NetworkInterfaceIds=[network_id]
                )["NetworkInterfaces"][0]["Association"]["PublicIp"]
                return send_message(
                    body, f"Minecraft server is online. At: {public_ip}"
                )
        return send_message(
            body, "Minecraft server is online, but I can't find its IP address."
        )
    if pending == 1:
        return send_message(body, "Minecraft server is starting up.")
    return send_message(body, "Unable to get status of Minecraft server.")


def ecs_start(body):
    """
    Sets the desired ECS tasks in the Minecraft service to 1.
    """
    response = ecs.describe_services(cluster=CLUSTER, services=[SERVICE])
    desired = response["services"][0]["desiredCount"]

    if desired == 0:
        ecs.update_service(cluster=CLUSTER, service=SERVICE, desiredCount=1)
        return send_message(body, "Starting Minecraft server.")
    return send_message(body, "Minecraft server already starting.")


def send_message(body, message):
    """
    Sends a message to discord.
    """
    application_id = body["application_id"]
    interaction_token = body["token"]
    follow_msg_url = (
        f"https://discord.com/api/v10/webhooks/{application_id}/{interaction_token}"
    )
    secretsmanager = boto3.client(service_name="secretsmanager")
    bot_token = secretsmanager.get_secret_value(SecretId="minecraft_discord_bot_token")[
        "SecretString"
    ]
    headers = {"Authorization": f"Bot {bot_token}", "Content-Type": "application/json"}
    http.request(
        "POST", follow_msg_url, headers=headers, body=json.dumps({"content": message})
    )
    return {"statusCode": 200}
