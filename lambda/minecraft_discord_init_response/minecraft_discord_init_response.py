"""
Receives a command from Discord via API Gateway

Due to Discord's requirement of a response within 3 seconds an acknowledgement needs to be sent
while offloading the work to a secondary Lambda.

"""

import json
import os

import boto3
from nacl.exceptions import BadSignatureError
from nacl.signing import VerifyKey

PUBLIC_KEY = os.environ["PUBLIC_KEY"]


def lambda_handler(event, context):  # pylint:disable=unused-argument
    """
    Verifies the integrity of an incoming message. Forwards message content to a secondary Lambda.
    Responds with an acknowledgement.
    """

    body = event["body"]

    signature = event["headers"]["x-signature-ed25519"]
    timestamp = event["headers"]["x-signature-timestamp"]
    verify_key = VerifyKey(bytes.fromhex(PUBLIC_KEY))

    try:
        verify_key.verify(f"{timestamp}{body}".encode(), bytes.fromhex(signature))
    except BadSignatureError:
        return {"statusCode": 401, "body": json.dumps("invalid request signature")}

    body = json.loads(event["body"])
    body_type = body["type"]

    if body_type == 1:
        return {"statusCode": 200, "body": json.dumps({"type": 1})}
    if body_type == 2:
        try:
            command_handler_lambda_arn = os.environ["COMMAND_HANDLER_LAMBDA_ARN"]
            lambda_client = boto3.client("lambda")
            lambda_client.invoke(
                FunctionName=command_handler_lambda_arn,
                InvocationType="Event",
                Payload=json.dumps(event),
            )
            return {"statusCode": 200, "body": json.dumps({"type": 5})}
        except OSError:
            return {
                "statusCode": 200,
                "body": json.dumps(
                    {
                        "type": 4,
                        "data": {
                            "content": "Cannot communicate with secondary Lambda.",
                        },
                    }
                ),
            }

    else:
        return {"statusCode": 400, "body": json.dumps("unhandled request type")}
