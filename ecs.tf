resource "aws_iam_policy" "efs_rw" {
  name        = "efs-rw-minecraft-data"
  path        = "/"
  description = "Policy for Minecraft EFS RW"
  policy      = data.aws_iam_policy_document.efs_rw.json
}

data "aws_iam_policy_document" "efs_rw" {
  statement {
    actions = [
      "elasticfilesystem:ClientMount",
      "elasticfilesystem:ClientWrite",
      "elasticfilesystem:DescribeMountTargets",
      "elasticfilesystem:DescribeFileSystems"
    ]
    resources = [aws_efs_file_system.this.arn]
    condition {
      test     = "StringEquals"
      variable = "elasticfilesystem:AccessPointArn"
      values   = [aws_efs_access_point.this.arn]
    }
  }
}

data "aws_iam_policy_document" "ecs_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "ecs_rw" {
  statement {
    actions = ["ecs:*"]
    resources = [
      "arn:aws:ecs:${var.region}:${data.aws_caller_identity.current.account_id}:service/minecraft/${local.minecraft_server}",
      "arn:aws:ecs:${var.region}:${data.aws_caller_identity.current.account_id}:task/${var.name}/*"
    ]
  }
  statement {
    actions = [
      "ec2:DescribeNetworkInterfaces"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "ecs_rw" {
  name        = "ecs-rw-minecraft-service"
  path        = "/"
  description = "Policy for Minecraft ECS RW"
  policy      = data.aws_iam_policy_document.ecs_rw.json
}

data "aws_iam_policy_document" "ecs_task_rw" {
  statement {
    actions = ["ecs:*"]
    resources = [
      "arn:aws:ecs:${var.region}:${data.aws_caller_identity.current.account_id}:service/minecraft/${local.minecraft_server}",
      "arn:aws:ecs:${var.region}:${data.aws_caller_identity.current.account_id}:task/${var.name}/*"
    ]
  }
  statement {
    actions = [
      "ec2:DescribeNetworkInterfaces",
      "ecs:DescribeTaskDefinition",
      "ecs:ListClusters",
      "ecs:ListContainerInstances",
      "ecs:DescribeContainerInstances",
      "ecs:ListTasks"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "ecs_task_rw" {
  name        = "ecs-task-rw-minecraft-service"
  path        = "/"
  description = "Policy for Minecraft ECS Task RW"
  policy      = data.aws_iam_policy_document.ecs_task_rw.json
}

data "aws_iam_policy_document" "mc_ecs_logging" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["arn:aws:logs:*:*:log-group:/aws/ecs/*"]

    principals {
      identifiers = ["ecs-tasks.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_cloudwatch_log_resource_policy" "mc_ecs_logging" {
  policy_name     = "ecs-minecraft-logging"
  policy_document = data.aws_iam_policy_document.mc_ecs_logging.json
}

resource "aws_iam_role" "ecs_task" {
  name               = "ecs-task-${local.minecraft_server}"
  assume_role_policy = data.aws_iam_policy_document.ecs_assume_role_policy.json
  managed_policy_arns = [
    aws_iam_policy.ecs_rw.arn,
    aws_iam_policy.efs_rw.arn,
    aws_iam_policy.sns.arn
  ]
}

resource "aws_iam_role" "ecs_task_execution" {
  name                = "ecs-task-execution-${local.minecraft_server}"
  assume_role_policy  = data.aws_iam_policy_document.ecs_assume_role_policy.json
  managed_policy_arns = [data.aws_iam_policy.AmazonECSTaskExecutionRolePolicy.arn]
}

resource "aws_cloudwatch_log_group" "minecraft_server" {
  name              = "/ecs/minecraft/${local.minecraft_server}"
  retention_in_days = 7
}



resource "aws_ecs_task_definition" "this" {
  family                   = local.minecraft_server
  requires_compatibilities = ["FARGATE"]
  task_role_arn            = aws_iam_role.ecs_task.arn
  execution_role_arn       = aws_iam_role.ecs_task_execution.arn
  network_mode             = "awsvpc"
  cpu                      = var.ecs_cpu
  memory                   = var.ecs_memory

  container_definitions = jsonencode([
    {
      name      = local.minecraft_server
      image     = local.minecraft_docker_image
      essential = false
      portMappings = [
        {
          containerPort = local.minecraft_port
          hostPort      = local.minecraft_port
          protocol      = local.minecraft_protocol
        },
        {
          containerPort = local.efs_port
          hostPort      = local.efs_port
          protocol      = local.efs_protocol
        }
      ]
      mountPoints = [
        {
          containerPath = "/data"
          sourceVolume  = "data"
        }
      ]
      environment = [
        {
          name  = "EULA"
          value = "TRUE"
        },
        {
          name  = "TYPE"
          value = var.minecraft_server_type
        },
        {
          name  = "MODRINTH_PROJECTS"
          value = var.minecraft_modrinth_projects
        },
        {
          name  = "MOTD"
          value = "A serverless Minecraft server!"
        },
        {
          name  = "DIFFICULTY"
          value = "normal"
        },
        {
          name  = "MODE"
          value = "survival"
        },
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = aws_cloudwatch_log_group.minecraft_server.name
          awslogs-region        = var.region
          awslogs-stream-prefix = "awslogs"
        }
      }
    },
    {
      name      = "minecraft-ecsfargate-watchdog"
      image     = "registry.gitlab.com/wagensveld/infra-minecraft:latest"
      essential = true
      environment = [
        {
          name  = "CLUSTER"
          value = "minecraft"
        },
        {
          name  = "SERVICE"
          value = local.minecraft_server
        },
        {
          name  = "SNSTOPIC"
          value = aws_sns_topic.this.arn
        },
        {
          name  = "STARTUPMIN"
          value = "10"
        }
      ]
    }
  ])

  volume {
    name = "data"
    efs_volume_configuration {
      file_system_id     = aws_efs_file_system.this.id
      transit_encryption = "ENABLED"
      authorization_config {
        access_point_id = aws_efs_access_point.this.id
        iam             = "ENABLED"
      }
    }
  }
}

resource "aws_ecs_cluster" "this" {
  name = var.name

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}

resource "aws_ecs_service" "this" {
  name             = local.minecraft_server
  cluster          = aws_ecs_cluster.this.id
  task_definition  = aws_ecs_task_definition.this.arn
  launch_type      = "FARGATE"
  platform_version = "1.4.0"

  network_configuration {
    subnets          = data.aws_subnets.default.ids
    security_groups  = [aws_security_group.this.id]
    assign_public_ip = true
  }
}
