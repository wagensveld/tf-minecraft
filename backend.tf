terraform {
  required_version = ">= 1.6"
  backend "s3" {
    bucket  = "svw-services-tfstate"
    key     = "tf-minecraft/terraform.tfstate"
    region  = "ap-southeast-2"
    encrypt = true
    # You may be expecting dynamodb_table here, but $3 a month is simply too expenisve for the convinience of not having my tfstate messed up.
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.18"
    }
  }
}
