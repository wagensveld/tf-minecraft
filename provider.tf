provider "aws" {
  region = var.region

  default_tags {
    tags = {
      creation    = "terraform"
      repo        = "infra-minecraft"
      Service     = "infra-minecraft"
      Environment = "Prod"
    }
  }
}
