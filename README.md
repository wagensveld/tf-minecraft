[![infracost](https://img.shields.io/endpoint?url=https://dashboard.api.infracost.io/shields/json/6a7a88b3-c61d-4b69-8e8f-8a582c33e08c/repos/e173ec8b-3b62-4301-a67a-a20f5c8237ba/branch/24251cb1-0d6f-4820-8f9a-d31c1e1d2c51)](https://dashboard.infracost.io/org/wagensveld/repos/e173ec8b-3b62-4301-a67a-a20f5c8237ba?tab=settings)

# Infra Minecraft

A fork of sorts of [Ray Gibson's minecraft-ondemand](https://github.com/doctorray117/minecraft-ondemand). Using a Discord bot to manage the provisioning of the Minecraft server rather than Route53.

The discord bot is based on [Kerolos Zaki's guide](https://github.com/ker0olos/aws-lambda-discord-bot).

## Overview

![infra_minecraft_infra diagram](diagrams/infra_minecraft.png)

When you send a slash command (e.g.`/start`) to the Discord bot that get's sent to the API Gateway which triggers the init_response Lambda.

Discord requires an initial response within 3 seconds. Afterwards the interaction token is made invalid. Because of this the init_response Lambda starts the command-handler Lambda, then gives Discord an acknowledgement.

The command-handler interprets and acts upon the command using boto3.

The ECS service contains two containers:

Minecraft Server: Contains the Minecraft server, at the moment it defaults to java.

The Watchdog: Monitors activity in the Minecraft container, if it doesn't detect any activity on the port associated with Minecraft (25565 for java; 19132 for bedrock) it scales the ECS service desired tasks to 0 after a set amount of time.

Connected to the ECS Service (and specifically the Minecraft Server container) is shared file storage via EFS.

Because EFS isn't friendly to interact with directly, files can be sycned to S3 via Datasync.

## Set Up

If you haven't already, [create an AWS account](https://aws.amazon.com/resources/create-account/).

Afterwards ensure you can apply Terraform to your AWS account. One method is [using Terraform Cloud with AWS via an OIDC identity](https://developer.hashicorp.com/terraform/cloud-docs/workspaces/dynamic-provider-credentials/aws-configuration).

A few variables are required to apply.

Aside the value of the discord_public_key the values don't matter too much.

- discord_public_key: From the bot set up below.
- email: For an SQS topic.
- zone_name: To name certain resources - such as the S3 bucket.

### Discord Bot

Go to the [Discord Developer Portal](https://discord.com/developers/applications). Create a new application. Take note of the `PUBLIC KEY` for the Terraform steps above.

Apply the Terraform code.

Take note of the API endpoint attached to the `minecraft_discord_init_response` Lambda.

In the settings for you Discord set the `Interactions Endpoint URL` to that value.

In your bot settings go to `OAuth2`, retrieve the client secret. Add it as the secret to the `minecraft_discord_bot_token` Secrets Manager secret. Take note of it as well for the following python code.

Locally create a python file. Its name doesn't matter. With the content:

```python
import requests

APP_ID = "REPLACE_ME"
SERVER_ID = "REPLACE_ME"
BOT_TOKEN = "REPLACE_ME"

# global commands are cached and only update every hour
url = f'https://discord.com/api/v10/applications/{APP_ID}/commands'

# while server commands update instantly
# they're much better for testing
#url = f'https://discord.com/api/v10/applications/{APP_ID}/guilds/{SERVER_ID}/commands'

json = [
  {
    'name': 'hello',
    'description': 'Test command.',
    'options': []
  },
  {
    'name': 'status',
    'description': 'Check status of the Minecraft server.',
    'options': []
  },
  {
    'name': 'start',
    'description': 'Start Minecraft server.',
    'options': []
  }
]

response = requests.put(url, headers={
  'Authorization': f'Bot {BOT_TOKEN}'
}, json=json)

print(response.json())
```

Replace the variables with their appropriate values. If you're just testing in a single server run the python file. If you want to propagate your commands globally comment out the first `url` variable, and uncomment the second.

Note: it takes an hour to propagate.
