resource "aws_efs_file_system" "this" {
  creation_token = var.name
  encrypted      = true

  tags = {
    Name = var.name
  }
}

resource "aws_efs_mount_target" "this" {
  for_each        = toset(data.aws_subnets.default.ids)
  file_system_id  = aws_efs_file_system.this.id
  subnet_id       = each.key
  security_groups = [data.aws_security_group.default.id]
}

resource "aws_efs_access_point" "this" {
  file_system_id = aws_efs_file_system.this.id
  posix_user {
    uid = 1000
    gid = 1000
  }
  root_directory {
    path = local.minecraft_subdirectory
    creation_info {
      owner_uid   = 1000
      owner_gid   = 1000
      permissions = 0755
    }
  }
}
