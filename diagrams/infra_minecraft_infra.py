"""
Generates a diagram of tf-minecraft
"""

from diagrams import Cluster, Diagram
from diagrams.custom import Custom
from diagrams.aws.network import APIGateway
from diagrams.aws.compute import ECS, ElasticContainerServiceContainer, Lambda
from diagrams.aws.storage import S3, EFS
from diagrams.aws.migration import Datasync

with Diagram("Infra Minecraft", show=False):
    discord = Custom("Discord", "./custom/discord.png")
    # Minecraft = Custom("Minecraft Java", "./custom/minecraft.png")

    with Cluster("AWS Cloud"):
        discord_lambda_gw = APIGateway("API Gateway")
        minecraft_discord_bot_lambda = Lambda("init_response\nLambda")
        minecraft_discord_bot_lambda_command_handler = Lambda("command-handler\nLambda")

        minecraft_files_s3 = S3("User Accessible\nMinecraft Files")
        data_sync = Datasync("Datasync")
        minecraft_files_efs = EFS("EFS Persistent Data")

        ecs_service = ECS("ECS Service")
        minecraft_container = ElasticContainerServiceContainer(
            "Minecraft Server Container"
        )
        watchdog_container = ElasticContainerServiceContainer("Watchdog Container")

    minecraft_files_s3 << data_sync << minecraft_files_efs  # pylint: disable=pointless-statement
    minecraft_files_s3 >> data_sync >> minecraft_files_efs  # pylint: disable=pointless-statement

    discord << discord_lambda_gw << minecraft_discord_bot_lambda  # pylint: disable=pointless-statement
    discord >> discord_lambda_gw >> minecraft_discord_bot_lambda  # pylint: disable=pointless-statement
    discord << minecraft_discord_bot_lambda_command_handler  # pylint: disable=pointless-statement
    minecraft_discord_bot_lambda >> minecraft_discord_bot_lambda_command_handler  # pylint: disable=pointless-statement
    minecraft_discord_bot_lambda_command_handler >> ecs_service  # pylint: disable=pointless-statement

    ecs_service >> minecraft_container  # pylint: disable=pointless-statement
    ecs_service >> watchdog_container  # pylint: disable=pointless-statement
    ecs_service << watchdog_container  # pylint: disable=pointless-statement
    ecs_service << minecraft_files_efs  # pylint: disable=pointless-statement
    ecs_service >> minecraft_files_efs  # pylint: disable=pointless-statement
