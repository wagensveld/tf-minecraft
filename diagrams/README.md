# Diagrams

Diagrams are handled by [mingrammer/diagrams](https://github.com/mingrammer/diagrams). They have installation instructions in their README, I've provided a `requirements.txt` for convenience.

```python
pip install -r requirements.txt
```

## infra_minecraft_infra.py

The star of the show. Covers the infrastructure of this repo.

![infra_minecraft_infra diagram](infra_minecraft.png)
