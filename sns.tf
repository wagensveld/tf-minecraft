resource "aws_sns_topic" "this" {
  name = "${var.name}-notifications"
}

resource "aws_sns_topic_subscription" "email" {
  topic_arn = aws_sns_topic.this.arn
  protocol  = "email"
  endpoint  = var.email
}

resource "aws_iam_policy" "sns" {
  name        = "sns-publish-minecraft-notifications"
  path        = "/"
  description = "Policy for Minecraft SNS"
  policy      = data.aws_iam_policy_document.sns.json
}

data "aws_iam_policy_document" "sns" {
  statement {
    actions = [
      "sns:Publish"
    ]
    resources = [aws_sns_topic.this.arn]
  }
}
