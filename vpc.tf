resource "aws_security_group" "this" {
  name        = "${var.name}-sg"
  description = "Security group for ECS Minecraft"
}

resource "aws_security_group_rule" "default_efs" {
  description              = "Allow connectivity to EFS"
  type                     = "ingress"
  from_port                = local.efs_port
  to_port                  = local.efs_port
  protocol                 = local.efs_protocol
  security_group_id        = data.aws_security_group.default.id
  source_security_group_id = aws_security_group.this.id
}

resource "aws_security_group_rule" "mc_minecraft" {
  description       = "Allow ingress to Minecraft ${var.minecraft_edition}"
  type              = "ingress"
  from_port         = local.minecraft_port
  to_port           = local.minecraft_port
  protocol          = local.minecraft_protocol
  security_group_id = aws_security_group.this.id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "mc_out" {
  description       = "Default AWS egress"
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.this.id
  cidr_blocks       = ["0.0.0.0/0"]
}
