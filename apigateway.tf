
resource "aws_api_gateway_rest_api" "lambda_discord" {
  name = "discord_lambda_gw"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "lambda_discord" {
  parent_id   = aws_api_gateway_rest_api.lambda_discord.root_resource_id
  path_part   = local.discord_init_response_name
  rest_api_id = aws_api_gateway_rest_api.lambda_discord.id
}

resource "aws_api_gateway_method" "lambda_discord" {
  authorization = "NONE"
  http_method   = "ANY"
  resource_id   = aws_api_gateway_resource.lambda_discord.id
  rest_api_id   = aws_api_gateway_rest_api.lambda_discord.id
}

resource "aws_api_gateway_integration" "lambda_discord" {
  http_method             = aws_api_gateway_method.lambda_discord.http_method
  resource_id             = aws_api_gateway_resource.lambda_discord.id
  rest_api_id             = aws_api_gateway_rest_api.lambda_discord.id
  type                    = "AWS_PROXY"
  integration_http_method = "POST"
  uri                     = aws_lambda_function.lambda_discord.invoke_arn
}

resource "aws_api_gateway_deployment" "lambda_discord" {
  rest_api_id = aws_api_gateway_rest_api.lambda_discord.id

  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.lambda_discord.id,
      aws_api_gateway_method.lambda_discord.id,
      aws_api_gateway_integration.lambda_discord.id,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "lambda_discord" {
  deployment_id = aws_api_gateway_deployment.lambda_discord.id
  rest_api_id   = aws_api_gateway_rest_api.lambda_discord.id
  stage_name    = "default"

  # access_log_settings {
  #   destination_arn = aws_cloudwatch_log_group.api_gw_discord.arn

  #   format = jsonencode({
  #     requestId               = "$context.requestId"
  #     sourceIp                = "$context.identity.sourceIp"
  #     requestTime             = "$context.requestTime"
  #     protocol                = "$context.protocol"
  #     httpMethod              = "$context.httpMethod"
  #     resourcePath            = "$context.resourcePath"
  #     routeKey                = "$context.routeKey"
  #     status                  = "$context.status"
  #     responseLength          = "$context.responseLength"
  #     integrationErrorMessage = "$context.integrationErrorMessage"

  #     domainName        = "$context.domainName"
  #     message           = "$context.error.message"
  #     messageString     = "$context.error.messageString"
  #     extendedRequestId = "$context.extendedRequestId"
  #     error             = "$context.integration.error"
  #     integrationStatus = "$context.integrationStatus"
  #     }
  #   )
  # }
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = aws_api_gateway_rest_api.lambda_discord.id
  resource_id = aws_api_gateway_resource.lambda_discord.id
  http_method = aws_api_gateway_method.lambda_discord.http_method
  status_code = "200"
}

resource "aws_api_gateway_method_response" "response_401" {
  rest_api_id = aws_api_gateway_rest_api.lambda_discord.id
  resource_id = aws_api_gateway_resource.lambda_discord.id
  http_method = aws_api_gateway_method.lambda_discord.http_method
  status_code = "401"
}

resource "aws_cloudwatch_log_group" "api_gw_discord" {
  name              = "/aws/api_gw/${aws_api_gateway_rest_api.lambda_discord.name}"
  retention_in_days = 7
}
