locals {
  discord_init_response_name = "minecraft_discord_init_response"
}

data "aws_iam_policy_document" "lambda_discord" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "lambda_lambda" {
  statement {
    actions = [
      "lambda:InvokeFunction"
    ]
    resources = [
      aws_lambda_function.lambda_discord_command_handler.arn
    ]
  }
}

resource "aws_iam_policy" "lambda_lambda" {
  name        = "lambda-lambda"
  path        = "/"
  description = "Allows lambda to start another lambda"
  policy      = data.aws_iam_policy_document.lambda_lambda.json
}

resource "aws_iam_role" "lambda_discord" {
  name               = "iam_for_minecraft-discord_lambda"
  assume_role_policy = data.aws_iam_policy_document.lambda_discord.json
  managed_policy_arns = [
    aws_iam_policy.lambda_lambda.arn,
    data.aws_iam_policy.AWSLambdaBasicExecutionRole.arn
  ]
}

data "aws_s3_object" "lambda_discord" {
  bucket = aws_s3_bucket.minecraft_files.bucket
  key    = "lambda/${local.discord_init_response_name}.zip"
}

data "aws_s3_object" "lambda_discord_sha" {
  bucket = aws_s3_bucket.minecraft_files.bucket
  key    = "lambda/${local.discord_init_response_name}.zip.sha256.txt"
}

resource "aws_cloudwatch_log_group" "lambda_discord" {
  name              = "/aws/lambda/${aws_lambda_function.lambda_discord.function_name}"
  retention_in_days = 7
}

resource "aws_lambda_function" "lambda_discord" {
  s3_bucket         = data.aws_s3_object.lambda_discord.bucket
  s3_key            = data.aws_s3_object.lambda_discord.key
  s3_object_version = data.aws_s3_object.lambda_discord.version_id
  handler           = "${local.discord_init_response_name}.lambda_handler"
  function_name     = local.discord_init_response_name
  role              = aws_iam_role.lambda_discord.arn
  source_code_hash  = chomp(data.aws_s3_object.lambda_discord_sha.body)
  runtime           = "python3.12"
  timeout           = 10

  environment {
    variables = {
      PUBLIC_KEY                 = var.discord_public_key
      COMMAND_HANDLER_LAMBDA_ARN = aws_lambda_function.lambda_discord_command_handler.arn
    }
  }
}

resource "aws_lambda_permission" "api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_discord.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.lambda_discord.execution_arn}/*/*/${local.discord_init_response_name}"
}

# output "api_gw_discord_endpoint" {
#   value = "${aws_apigatewayv2_api.lambda_discord.api_endpoint}/default/${local.discord_init_response_name}"
# }
